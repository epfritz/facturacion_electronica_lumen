<?php

namespace App\Http\Controllers;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\App;
use DateTime;

class ErrorException extends \Exception { }

class SiiController extends Controller
{
    

    private $token;
    private $config;

    ################################################
    ## Variables permitidas para comunicación con ##
    ## Web Service Consulta y Registro de         ##
    ## Aceptación/Reclamo a DTE recibido          ##
    ################################################

    /*
    Tipo Documento

    33: Factura Electrónica.
    34: Factura No Afecta o Exenta Electrónica.
    43: Liquidación Factura Electrónica.

    */

    private $tipos_permitiods = array(33,34,43);


    /*
    Accion [ingresarAceptacionReclamoDoc]

    ACD: Acepta Contenido del Documento
    RCD: Reclamo al Contenido del Documento
    ERM: Otorga Recibo de Mercaderías o Servicios
    RFP: Reclamo por Falta Parcial de Mercaderías
    RFT: Reclamo por Falta Total de Mercaderías

    */
    private $acctiones_permitiods = array('ACD','RCD','ERM','RFP','RFT');
    private $rechazos = array('RFP','RFT','RCD');


    //Obtenemos url del wsdl dependiendo del ambiente
    private $wsdl;

    private $empresas = array(
        array("receptor" => "86906100-K","id" => 2),
        array("receptor" => "85297000-6","id" => 3),
        array("receptor" => "93688000-2","id" => 1)
    );

    private $sucursales = [
            "OSORNO" => 1,
            "PUERTO MONTT" => 2,
            "VALDIVIA" => 3,
            "CASTRO" => 4,
            "LA UNION" => 7,
            "ANCUD" => 9
        ];

    private $direcciones = [
            "FUCHSLOCHER" => 1,
            "PILPILCO" => 2,
            "PICARTE" => 3,
            "RUTA" => 4,
            "MATTA" => 6,
            "GROBB" => 7,
            "PRAT" => 9
        ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $config = array(
            'firma' => array(
                'pass' => env('CLAVE_CERTIFICADO'),
                'file' => realpath(__DIR__.env('CERTIFICADO'))
            )
        );

        //Ambiente Sii
        (bool)(App::environment('local'))?\sasco\LibreDTE\Sii::setAmbiente(\sasco\LibreDTE\Sii::CERTIFICACION):\sasco\LibreDTE\Sii::setAmbiente();
        $this->config = $config;

        //Obtenemos Token
        $token = \sasco\LibreDTE\Sii\Autenticacion::getToken($this->config['firma']);
        if (!$token) {
            foreach (\sasco\LibreDTE\Log::readAll() as $error)
                echo $error,"\n";
            exit;
        }else{
            $this->token = $token;
        }

        //Seteamos WSDL
        $this->wsdl = App::environment('local')?env('WSDL_DEV'):env('WSDL_PROD');

        header('Access-Control-Allow-Origin: *'); 
        
        
    }

    public function check_conection(){

        dd("Token: ".$this->token);
    }

    /**
     * Método para enviar aceptacin de documento
     * @param Receptor formato 17897012-7,Emisor formato 17897012-7 , $action = ACD | RCD | ERM | RFP | RFT
     * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
     * @version 2017-08-17
     */
    public function ingresarAceptacionReclamoDoc($Emisor,$tipo_doc,$folio,$Receptor,$accionDoc){

        $state = 0;
        $xml = "";
        $message = "";

        //Obtenemos entidad receptora
        $receptor = $this->getReceptor($Receptor);

        // datos para validar
        $rutEmisor = explode('-', $Emisor); 
        $dte = $tipo_doc.'_'.$folio.'_'.$Emisor.'.xml';

        $dte_route = env('DTES_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte;
        if(Flysystem::connection('local')->has($dte_route)){

            //Opciones de ennvio para Cliente Soap. 
            $options = array(
                "exceptions" => 0, 
                "trace" => 1,
                'stream_context' => stream_context_create(
                    array(
                        'http' => array(
                            'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                        )
                    )
                )
            );
            
            $soap = new \SoapClient($this->wsdl, $options);
           
            $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
            $EnvioDte->loadXML(Flysystem::connection('local')->read($dte_route));
            $documentos = $EnvioDte->getDocumentos();

            foreach ($documentos as $documento) {

                if(in_array($tipo_doc,$tipos_permitiods) && in_array($accionDoc,$acctiones_permitiods)){
                    if($folio == $documento->getFolio() && $Emisor == $documento->getEmisor() && $Receptor == $documento->getReceptor() && $tipo_doc == $documento->getTipo()){
                        //Parametros de función ingresarAceptacionReclamoDoc  
                        $args = [
                            'rutEmisor' => $rutEmisor[0],
                            'dvEmisor' => $rutEmisor[1],
                            'tipoDoc' => $tipo_doc,
                            'folio' => $folio,
                            'accionDoc' => $accionDoc
                        ];

                        $retry = env('RETRY_CALL');

                        for ($i=0; $i<$retry; $i++) {
                            try {
                                $body = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args);
                                break;
                            } catch (\Exception $e) {
                                $msg = $e->getMessage();
                                if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                                    $msg .= ': '.$e->getTrace()[0]['args'][1];
                                }
                                \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                                $body = null;
                            }
                        }

                        if ($body===null) {
                            \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
                            $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

                        }else{
                            if(!$body->codResp != 0){
                                $message = $body->descResp;
                            }else{
                                $message = $body->descResp;
                                 //Movmeos xml a carpeta de enviados
                                $dte_sent_route = env('DTES_ENVIADOS_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte;
                                if(!Flysystem::connection('local')->has($dte_sent_route)){
                                    if(Flysystem::connection('local')->copy($dte_route,$dte_sent_route)){
                                        Flysystem::connection('local')->delete($dte_route);
                                        $state = 1;
                                    }else{
                                        $message = "Acuse enviado , sin embargo no pudo moverse el archivo";        
                                    }
                                }else{
                                    $message = "Acuse enviado , sin embargo el archivo ya existe en carpeta enviados";        
                                }
                            }
                        }
                    }else{
                        $message = "Datos de cabecera no coinciden";
                    }
                }else{
                    $message = "Tipo o Acción no permitidos";
                }
            }            
        }else{
            $message = "DTE no Registrado o Aviso ya enviado";
        }

        return response()->json(array("state" => $state,"message" => $message,"xml" => $xml));

    }

    /**
     * Método para enviar acuse de recibo de mercaderia de forma masiva
     * @param Receptor formato 17897012-7,Emisor formato 17897012-7
     * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
     * @version 2017-08-17
     */
    public function acuseReciboMercaderiaMasivo(){

        $state = 0;
        $xml = "";
        $message = "";
        $errors = array();

        $countTotal = 0;
        $countSuccess = 0;

        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );

        $soap = new \SoapClient($this->wsdl, $options);
        
        $retry = env('RETRY_CALL');

        //Recorremos toda la carpeta de pendientes
        $folder = Flysystem::connection('local')->get(env('DTES_CARPETA'));
        $sub_folders = $folder->getContents();

        $emisores = array();
        //obtenemos la lista de los receptores automáticos
        $ruts = json_decode(trim(file_get_contents(env('API').'/'.env('API_RUT')), "\xEF\xBB\xBF"),true);

        foreach ($ruts as $rut) {
            array_push($emisores, $rut["RUT"]);
        }

        foreach ($sub_folders as $key => $value) {
            if($value["type"] == "dir"){
                $sub_folder = Flysystem::connection('local')->get($value["path"]);
                $dtes = $sub_folder->getContents();
                foreach ($dtes as $dte) {
                    $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
                    $EnvioDte->loadXML(Flysystem::connection('local')->read($dte["path"]));
                    $documentos = $EnvioDte->getDocumentos();

                    foreach ($documentos as $documento) {

                        $receptor = $this->getReceptor($documento->getReceptor());
                        
                        //Verificamos si es pago contado
                        $formaPago = isset($documento->getDatos()["Encabezado"]["IdDoc"]["FmaPago"])?(int)$documento->getDatos()["Encabezado"]["IdDoc"]["FmaPago"]:false;
                        $contado = $formaPago?($formaPago == 1):false;

                        ######################################
                        ## PARA PAGO CONTADO O TIPO 56 O 61 ##
                        ######################################
                        
                        if($contado || $documento->getTipo() == 61 || $documento->getTipo() == 56){
                            $countTotal++;

                            $dte_sent_route = env('DTES_ENVIADOS_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte["basename"];
                            if(!Flysystem::connection('local')->has($dte_sent_route)){
                                if(Flysystem::connection('local')->copy($dte["path"],$dte_sent_route)){
                                    Flysystem::connection('local')->delete($dte["path"]);
                                    $countSuccess++;

                                    #################################
                                    ## Actualizamos estado del DTE ##
                                    #################################

                                    //Empresa
                                    $empresa = $this->getIdByReceptor($documento->getReceptor());
                                    $parms = array(
                                        ':i_empresa' => $empresa,
                                        ':i_tipo' => $documento->getTipo(),
                                        ':i_numero' => $documento->getFolio(),
                                        ':i_fechasii' => date('d/m/Y H:i:s'),
                                        ':i_tiposii' => 'PAGO CONTADO'
                                    );

                                    $ruta = env('API').'/'.env('API_ENVIO');
                                    $parms = http_build_query($parms);
                                    $query = $ruta.'?'.$parms;
                                    $api = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);
                                        
                                }else{
                                    $message = "Acuse enviado , sin embargo no pudo moverse el archivo";
                                    $error = array($documento->getFolio() => $message);
                                    array_push($errors,$error); 
                                }
                            }else{
                                $message = "Acuse enviado , sin embargo el archivo ya existe en carpeta enviados";
                                $error = array($documento->getFolio() => $message);
                                array_push($errors,$error);     
                            } 
                        }

                        //Verificamos forma de pago
                        $formaPago = isset($documento->getDatos()["Encabezado"]["IdDoc"]["FmaPago"])?(int)$documento->getDatos()["Encabezado"]["IdDoc"]["FmaPago"]:false;
                        $formaPago = $formaPago?($formaPago != 1):true;

                        //Verificamos que el emisor este en la lista 
                        if(in_array($documento->getEmisor(),$emisores) && in_array((int)$documento->getTipo(),$this->tipos_permitiods) && $formaPago){
                            $countTotal++;

                            //Enviamos acepta documento
                            $rut = explode('-',$documento->getEmisor());
                            $args1 = [
                                'rutEmisor' => $rut[0],
                                'dvEmisor' => $rut[1],
                                'tipoDoc' => $documento->getTipo(),
                                'folio' => $documento->getFolio(),
                                'accionDoc' => 'ACD'
                            ];

                            //Enviamos acuse recibo mercadería
                            $args2 = [
                                'rutEmisor' => $rut[0],
                                'dvEmisor' => $rut[1],
                                'tipoDoc' => $documento->getTipo(),
                                'folio' => $documento->getFolio(),
                                'accionDoc' => 'ERM'
                            ];

                            for ($i=0; $i<$retry; $i++) {
                                try {
                                    $body1 = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args1);
                                    break;
                                } catch (\Exception $e) {
                                    $msg = $e->getMessage();
                                    if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                                        $msg .= ': '.$e->getTrace()[0]['args'][1];
                                    }
                                    \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                                    $body1 = null;
                                    $error = array($documento->getFolio() => \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP,$msg));
                                    array_push($errors,$error);
                                }
                            }

                            for ($i=0; $i<$retry; $i++) {
                                try {
                                    $body2 = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args2);
                                    break;
                                } catch (\Exception $e) {
                                    $msg = $e->getMessage();
                                    if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                                        $msg .= ': '.$e->getTrace()[0]['args'][1];
                                    }
                                    \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                                    $body2 = null;
                                    $error = array($documento->getFolio() => \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP,$msg));
                                    array_push($errors,$error);
                                }
                            }


                            if($body1 != null && $body2 !== null ){

                                if($body1->codResp == 0 && $body2->codResp == 0){

                                    $dte_sent_route = env('DTES_ENVIADOS_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte["basename"];
                                    if(!Flysystem::connection('local')->has($dte_sent_route)){
                                        if(Flysystem::connection('local')->copy($dte["path"],$dte_sent_route)){
                                            Flysystem::connection('local')->delete($dte["path"]);
                                            $countSuccess++;

                                            #################################
                                            ## Actualizamos estado del DTE ##
                                            #################################

                                            //Empresa
                                            $empresa = $this->getIdByReceptor($documento->getReceptor());
                                            $parms = array(
                                                ':i_empresa' => $empresa,
                                                ':i_tipo' => $documento->getTipo(),
                                                ':i_numero' => $documento->getFolio(),
                                                ':i_fechasii' => date('d/m/Y H:i:s'),
                                                ':i_tiposii' => 'ERM - ACD'
                                            );

                                            $ruta = env('API').'/'.env('API_ENVIO');
                                            $parms = http_build_query($parms);
                                            $query = $ruta.'?'.$parms;
                                            $api = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);
                                                
                                        }else{
                                            $message = "Acuse enviado , sin embargo no pudo moverse el archivo";
                                            $error = array($documento->getFolio() => $message);
                                            array_push($errors,$error); 
                                        }
                                    }else{
                                        $message = "Acuse enviado , sin embargo el archivo ya existe en carpeta enviados";
                                        $error = array($documento->getFolio() => $message);
                                        array_push($errors,$error);     
                                    }    
                                }else{

                                    if($body1->codResp != 0){
                                        $error = array($documento->getFolio() => $body1->descResp);
                                        array_push($errors,$error);
                                    }

                                    if($body2->codResp != 0){
                                        $error = array($documento->getFolio() => $body2->descResp);
                                        array_push($errors,$error);
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        
        //Cuando no hay documentos Total es 0

        return response()->json(array("total" => $countTotal,"correcto" => $countSuccess,"errors" => $errors));

    }

    /**
     * Método para enviar aceptacin de documento
     * @param Receptor formato 17897012-7,Emisor formato 17897012-7
     * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
     * @version 2017-08-17
     */
    public function aceptaDocumento($Emisor,$tipo_doc,$folio,$Receptor){

        $state = 0;
        $xml = "";
        $message = "";

        //Obtenemos entidad receptora
        $receptor = $this->getReceptor($Receptor);

        // datos para validar
        $rutEmisor = explode('-', $Emisor); 
        $dte = $tipo_doc.'_'.$folio.'_'.$Emisor.'.xml';

        $dte_route = env('DTES_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte;
         //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
        $soap = new \SoapClient($this->wsdl, $options);
        # variable para diferencia el ingreso manual desde el xml
        $manual = isset($_GET['manual']) ? true : false;
        if(Flysystem::connection('local')->has($dte_route)){

            $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
            $EnvioDte->loadXML(Flysystem::connection('local')->read($dte_route));
            $documentos = $EnvioDte->getDocumentos();

            foreach ($documentos as $documento) {


                if($folio == $documento->getFolio() && $Emisor == $documento->getEmisor() && $Receptor == $documento->getReceptor() && $tipo_doc == $documento->getTipo()){
                    //Parametros de función ingresarAceptacionReclamoDoc  
                    $args = [
                        'rutEmisor' => $rutEmisor[0],
                        'dvEmisor' => $rutEmisor[1],
                        'tipoDoc' => $tipo_doc,
                        'folio' => $folio,
                        'accionDoc' => 'ACD'
                    ];


                    $retry = env('RETRY_CALL');

                    for ($i=0; $i<$retry; $i++) {
                        try {
                            $body = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args);
                            break;
                        } catch (\Exception $e) {
                            $msg = $e->getMessage();
                            if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                                $msg .= ': '.$e->getTrace()[0]['args'][1];
                            }
                            \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                            $body = null;
                        }
                    }

                    if ($body===null) {
                        \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
                        $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

                    }else{
                        $message = $body->descResp;
                        $state = $body->codResp;
                    }
                }else{
                    $message = "Datos de cabecera no coinciden";
                }
            }            
         }else if($manual){

            $args = [
                'rutEmisor' => $rutEmisor[0],
                'dvEmisor' => $rutEmisor[1],
                'tipoDoc' => $tipo_doc,
                'folio' => $folio,
                'accionDoc' => 'ACD'
            ];

            $retry = env('RETRY_CALL');

            for ($i=0; $i<$retry; $i++) {
                try {
                    $body = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args);
                    break;
                } catch (\Exception $e) {
                    $msg = $e->getMessage();
                    if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                        $msg .= ': '.$e->getTrace()[0]['args'][1];
                    }
                    \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                    $body = null;
                }
            }

            if ($body===null) {
                \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
                $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

            }else{
                $message = $body->descResp;
                $state = $body->codResp;
            }

       } else{
            $message = "DTE no Registrado o Aviso ya enviado";
        }


        return response()->json(array("state" => $state,"message" => $message,"xml" => $xml));

    }

    /**
     * Método para énviar acuse de recibo de mercadería
     * @param Receptor formato 17897012-7,Emisor formato 17897012-7
     * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
     * @version 2017-08-17
     */
    public function acuseReciboMercaderia($Emisor,$tipo_doc,$folio,$Receptor){

        $state = 0;
        $xml = "";
        $message = "";

        //Obtenemos entidad receptora
        $receptor = $this->getReceptor($Receptor);

        // datos para validar
        $rutEmisor = explode('-', $Emisor); 
        $dte = $tipo_doc.'_'.$folio.'_'.$Emisor.'.xml';

        $dte_route = env('DTES_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte;
         $manual = isset($_GET['manual']) ? true : false;
        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
        
        $soap = new \SoapClient($this->wsdl, $options);

        if(Flysystem::connection('local')->has($dte_route)){
           
            $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
            $EnvioDte->loadXML(Flysystem::connection('local')->read($dte_route));
            $documentos = $EnvioDte->getDocumentos();

            foreach ($documentos as $documento) {


                if($folio == $documento->getFolio() && $Emisor == $documento->getEmisor() && $Receptor == $documento->getReceptor() &&  $tipo_doc == $documento->getTipo()){
                    //Parametros de función ingresarAceptacionReclamoDoc  
                    $args = [
                        'rutEmisor' => $rutEmisor[0],
                        'dvEmisor' => $rutEmisor[1],
                        'tipoDoc' => $tipo_doc,
                        'folio' => $folio,
                        'accionDoc' => 'ERM'
                    ];

                    $retry = env('RETRY_CALL');

                    for ($i=0; $i<$retry; $i++) {
                        try {
                            $body = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args);
                            break;
                        } catch (\Exception $e) {
                            echo "catch";
                            $msg = $e->getMessage();
                            if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                                $msg .= ': '.$e->getTrace()[0]['args'][1];
                            }
                            \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                            $body = null;
                        }
                    }

                    if ($body===null) {
                        \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
                        $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

                    }else{
                        //if($body->codResp != 0){
                        if(false){
                            $message = $body->descResp;
                        }else{
                            $message = $body->descResp;
                             //Movmeos xml a carpeta de enviados
                            $dte_sent_route = env('DTES_ENVIADOS_CARPETA').'/'.env('DTES_CARPETA_'.$receptor).'/'.$dte;
                            if(!Flysystem::connection('local')->has($dte_sent_route)){
                                if(Flysystem::connection('local')->copy($dte_route,$dte_sent_route)){
                                    Flysystem::connection('local')->delete($dte_route);
                                    //$state = $body->codResp;
                                    $state = 0;
                                    #################################
                                    ## Actualizamos estado del DTE ##
                                    #################################

                                    //Empresa
                                    /*$empresa = $this->getIdByReceptor($documento->getReceptor());
                                    $parms = array(
                                        ':i_empresa' => $empresa,
                                        ':i_tipo' => $documento->getTipo(),
                                        ':i_numero' => $documento->getFolio(),
                                        ':i_fechasii' => date('d/m/Y').' 00:00:00',
                                        ':i_tiposii' => 'ERM'
                                    );

                                    $ruta = env('API').'/'.env('API_ENVIO');
                                    $parms = http_build_query($parms);
                                    $query = $ruta.'?'.$parms;
                                    $api = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);*/

                                }else{
                                    $message = "Acuse enviado , sin embargo no pudo moverse el archivo";        
                                }
                            }else{
                                $message = "Acuse enviado , sin embargo el archivo ya existe en carpeta enviados";        
                            }
                        }
                    }
                }else{
                    $message = "Datos de cabecera no coinciden";
                }
            }            
       }else if($manual){
            $args = [
                'rutEmisor' => $rutEmisor[0],
                'dvEmisor' => $rutEmisor[1],
                'tipoDoc' => $tipo_doc,
                'folio' => $folio,
                'accionDoc' => 'ERM'
            ];

            $retry = env('RETRY_CALL');

            for ($i=0; $i<$retry; $i++) {
                try {
                    $body = $soap->__soapCall("ingresarAceptacionReclamoDoc",$args);
                    break;
                } catch (\Exception $e) {
                    echo "catch";
                    $msg = $e->getMessage();
                    if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                        $msg .= ': '.$e->getTrace()[0]['args'][1];
                    }
                    \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                    $body = null;
                }
            }

            if ($body===null) {
                \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
                $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

            }else{
                $message = $body->descResp;
            }
        }else{
            $message = "DTE no Registrado o Aviso ya enviado";
        }
        return response()->json(array("state" => $state,"message" => $message,"xml" => $xml));

    }

    public function checkAcuse($Emisor,$tipo_doc,$folio) {

        $state = 0;
        $message = "";

        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
        
        $soap = new \SoapClient($this->wsdl, $options);

        $rutEmisor = explode('-', $Emisor); 
        
        $args = [
            'rutEmisor' => $rutEmisor[0],
            'dvEmisor' => $rutEmisor[1],
            'tipoDoc' => $tipo_doc,
            'folio' => $folio
        ];

        $retry = env('RETRY_CALL');

        for ($i=0; $i<$retry; $i++) {
            try {
                $body = $soap->__soapCall("listarEventosHistDoc",$args);
                break;
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                    $msg .= ': '.$e->getTrace()[0]['args'][1];
                }
                \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                $body = null;
            }
        }
        if ($body===null) {
            \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
            $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

        }else{
            $message = $body;
        }

        $state = $message->codResp;
        $tiposii= "";
        if($state == 15){ 

            if(is_array($message->listaEventosDoc) && count($message->listaEventosDoc) > 1){
                $tiposii = $message->listaEventosDoc[1]->codEvento;
            }else{
                $tiposii = $message->listaEventosDoc->codEvento;
            }
        }
        
        return response()->json(array("state" => $state,"message" => $message,"folio"=>$folio,"emisor"=>$Emisor,"tipo"=>$tipo_doc,"tiposii"=>$tiposii));
    }

    /**
     * Método para descargar correos de dtes
     * @param Receptor formato 17897012-7
     * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
     * @version 2017-08-17
     */
    public function mailDownload(){

        $countRecibidos = 0;
        $countRechazados = 0;
        //Se descargan mails de las empresas en el arreglo
        foreach ($this->empresas as $empresa) {

            $receptor = $this->getReceptor($empresa["receptor"]);
            
            $inbox  = imap_open (env('MAIL_HOST'), env('MAIL_USER_'.$receptor), env('MAIL_PWD_'.$receptor));
            imap_errors();
            $emails = imap_search($inbox,'ALL');
           
            if($emails) {

                rsort($emails);

                foreach($emails as $email_number) {

                    $overview = imap_fetch_overview($inbox,$email_number,0);
                    
                    $header = imap_headerinfo($inbox,$email_number,0);

                    $message = imap_fetchbody($inbox,$email_number,2);

                    $structure = imap_fetchstructure($inbox, $email_number);

                    $attachments = array();

                    if(isset($structure->parts) && count($structure->parts)) {
                        for($i = 0; $i < count($structure->parts); $i++) {
                            $attachments[$i] = array(
                                'is_attachment' => false,
                                'filename' => '',
                                'name' => '',
                                'attachment' => ''
                            );

                            if($structure->parts[$i]->ifdparameters) {
                                foreach($structure->parts[$i]->dparameters as $object) {
                                    if(strtolower($object->attribute) == 'filename') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['filename'] = $object->value;
                                    }
                                }
                            }

                            if($structure->parts[$i]->ifparameters) {
                                foreach($structure->parts[$i]->parameters as $object) {
                                    if(strtolower($object->attribute) == 'name') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['name'] = str_replace("(formato SII)", "", $object->value);
                                    }
                                }
                            }

                            if($attachments[$i]['is_attachment']){
                                $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i+1);

                                if($structure->parts[$i]->encoding == 3) { 
                                    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                }
                                elseif($structure->parts[$i]->encoding == 4) { 
                                    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                }
                            }
                        }
                    }

                    foreach($attachments as $attachment) {

                        if($attachment['is_attachment'] == 1) {
                        
                            $filename = $attachment['name'];
                            $pos      = strpos($filename, "_receptor");
                            $endpoint = $pos + strlen("_receptor");                    
                            $filename = str_replace(array("_receptor", "dt_tipo-"),"",substr($filename,0,$endpoint )).".xml";
                            $filename = str_replace(array("_folio-","_emisor-"), "_", $filename);
                            
                            if(empty($filename)) $filename = $attachment['filename'];

                            if(empty($filename)) $filename = time() . ".dat";
                            
                            $xml_route = env('DTES_CARPETA')."/".env('DTES_CARPETA_'.$receptor)."/".$filename;
                            if(Flysystem::connection('local')->put($xml_route,$attachment["attachment"])){
                            

                                //Nuevo objeto DTE
                                $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
                                $EnvioDte->loadXML(Flysystem::connection('local')->read($xml_route));
                                $documentos = $EnvioDte->getDocumentos();

                                $documento = $documentos[0];

                                //Insertamos DTE en bd

                                $emision = \DateTime::createFromFormat('Y-m-d', $documento->getFechaEmision());

                                //Consultamos sucursal recepción 
                                $ruta = env('API').'/'.env('API_SUC');
                                $parms = array(
                                    'rut' => $documento->getEmisor()
                                );

                                $parms = http_build_query($parms);
                                $query = $ruta.'?'.$parms;
                                $id_suc = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);

                                    
                                //Verificamos que no tenga suc
                                if($id_suc == 0){
                                    //Buscamos comuna de receptor
                                    $varcmnarecep = isset($documento->getDatos()["Encabezado"]["Receptor"]["CmnaRecep"]) ? $documento->getDatos()["Encabezado"]["Receptor"]["CmnaRecep"] :null;
                                    $comunaReceptor = strtoupper($varcmnarecep);
                                    //print_r(json_encode($comunaReceptor));
                                    $id_suc = isset($this->sucursales[$comunaReceptor])?$this->sucursales[$comunaReceptor]:0;

                                    $dirReceptor = isset($documento->getDatos()["Encabezado"]["Receptor"]["DirRecep"])?$documento->getDatos()["Encabezado"]["Receptor"]["DirRecep"]:false;

                                    if($id_suc == 1){
                                        if($dirReceptor){
                                            if(strpos(strtoupper($dirReceptor), 'MATTA') !== false){
                                               //En case de ser OSORNO y tener la palabra MATTA en la dirección , se asigna sucursal 6
                                                $id_suc = 6;
                                            }
                                        }
                                    }

                                    //En caso de no encontrar direccion por comuna, se busca por direccion de receptor
                                    if($id_suc == 0){
                                        if($dirReceptor){
                                            $direcciones = $this->direcciones;
                                            if($direcciones){
                                                foreach ($direcciones as $direccion => $idDireccion) {
                                                    if(strpos(strtoupper($dirReceptor), $direccion) !== false){
                                                        //En caso de coincidir con alguna de las direcciones
                                                        $id_suc = $idDireccion;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }

                                //Buscamos datos Emisor
                                $comunaEmisor = isset($documento->getDatos()["Encabezado"]["Emisor"]["CmnaOrigen"])?$documento->getDatos()["Encabezado"]["Emisor"]["CmnaOrigen"]:'';
                                $dirEmisor = isset($documento->getDatos()["Encabezado"]["Emisor"]["DirOrigen"])?$documento->getDatos()["Encabezado"]["Emisor"]["DirOrigen"]:'';
                                $nombreEmisor = isset($documento->getDatos()["Encabezado"]["Emisor"]["RznSoc"])?$documento->getDatos()["Encabezado"]["Emisor"]["RznSoc"]:'';

                                //Fecha de mail
                                $date = new \DateTime($header->date);
                                $fechadate = $date->format('d/m/Y H:i:s');
                                $parms = array(
                                    ':i_empresa' => $empresa["id"],
                                    ':i_tipo' => $documento->getTipo(),
                                    ':i_numero' => $documento->getFolio(),
                                    ':i_fecha' => $emision->format('d/m/Y').' 00:00:00',
                                    ':i_emisor' => $documento->getEmisor(),
                                    ':i_sucursal' => $id_suc,
                                    ':i_enviosii' => 0,
                                    ':i_usuariosii' => '',
                                    ':i_fechasii' => '',
                                    ':i_tiposii' => '',
                                    ':i_contabiliza' => 0,
                                    ':i_fechaconta' => '',
                                    ':i_usuarioconta' => '',
                                    ':i_nombreemisor' => $nombreEmisor,
                                    ':i_diremisor' => $dirEmisor,
                                    ':i_comunaemisor' => $comunaEmisor,
                                    ':i_fechamail' => $fechadate,
                                    ':i_total' => $documento->getMontoTotal()
                                );

                                #Se envia a Conta para su ingreso en el maestro
                                $ruta = env('API').'/'.env('API_INSERT');
                                $parms = http_build_query($parms);
                                $query = $ruta.'?'.$parms;
                                $api = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);

                                #Nuevo Proceso, en caso de ser un documento emitido desde GMAC
                                if($api['data'][':i_emisor'] == "86914600-5" && $api['data'][':i_tipo'] == '33') {
                                    #Array a enviar al sistema de Contabilidad
                                    $tosend  = array();
                                    #Leemos el XML y se completa la info del documento
                                    $getData = $this->getInfoDocGMAC($api['data']);
                                    #Se completa el array a enviar
                                    $tosend  = array(
                                        "preData" => $api['data'],
                                        "docData" => $getData['documentos']
                                    );
                                    #Envio de datos al Sistema de Contabilidad para registrar el Dte                                    
                                    $ruta = env('API').'/home/insertGmac';
                                    $data = http_build_query($tosend);
                                    $query = $ruta.'?'.$data;
                                    $api = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);
                                    //dd($api);
                                }

                                //dd($api['data'][':i_emisor']);
                                if(!App::environment('local')){
                                    imap_delete($inbox,$email_number);
                                }

                                #Contador de Documentos Correctamente Registrados
                                if($api["state"]){
                                    $countRecibidos++;
                                }
                            }
                        }
                    }
                }
            } 

            imap_expunge($inbox);
            imap_close($inbox);
        }

    
        //Se abre buzón de dte Rechazados
        $inbox  = imap_open (env('MAIL_HOST'), env('MAIL_USER_RECHAZADOS',OP_HALFOPEN), env('MAIL_PWD_RECHAZADOS'));
        imap_errors();
        $emails = imap_search($inbox,'ALL');
        if($emails) {

            rsort($emails);

            foreach($emails as $email_number) {

                $overview = imap_fetch_overview($inbox,$email_number,0);
                
                $header = imap_headerinfo($inbox,$email_number,0);

                $message = imap_fetchbody($inbox,$email_number,2);

                $structure = imap_fetchstructure($inbox, $email_number);

                $attachments = array();

                if(isset($structure->parts) && count($structure->parts)) {
                    for($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );

                        if($structure->parts[$i]->ifdparameters) {
                            foreach($structure->parts[$i]->dparameters as $object) {
                                if(strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if($structure->parts[$i]->ifparameters) {
                            foreach($structure->parts[$i]->parameters as $object) {
                                if(strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = str_replace("(formato SII)", "", $object->value);
                                }
                            }
                        }

                        if($attachments[$i]['is_attachment']){
                            $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i+1);

                            if($structure->parts[$i]->encoding == 3) { 
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            }
                            elseif($structure->parts[$i]->encoding == 4) { 
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }

                foreach($attachments as $attachment) {

                    if($attachment['is_attachment'] == 1) {
                    
                        $filename = $attachment['name'];
                        $pos      = strpos($filename, "_receptor");
                        $endpoint = $pos + strlen("_receptor");                    
                        $filename = str_replace(array("_receptor", "dt_tipo-"),"",substr($filename,0,$endpoint )).".xml";
                        $filename = str_replace(array("_folio-","_emisor-"), "_", $filename);
                        
                        if(empty($filename)) $filename = $attachment['filename'];

                        if(empty($filename)) $filename = time() . ".dat";
                        
                        $xml_route = env('DTES_RECHAZADOS')."/".$filename;
                        if(Flysystem::connection('local')->put($xml_route,$attachment["attachment"])){
                        

                            //Nuevo objeto DTE
                            $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
                            $EnvioDte->loadXML(Flysystem::connection('local')->read($xml_route));
                            $documentos = $EnvioDte->getDocumentos();

                            $documento = $documentos[0];

                            
                            $parms = array(
                                ':ii_emisor' => $documento->getEmisor(),
                                ':ii_tipo' => $documento->getTipo(),
                                ':ii_numero' => $documento->getFolio()
                            );

                            $ruta = env('API').'/'.env('API_RECHAZADO');
                            $parms = http_build_query($parms);
                            $query = $ruta.'?'.$parms;
                            $api = json_decode(trim(file_get_contents($query), "\xEF\xBB\xBF"),true);
                            if(!App::environment('local')){
                                imap_delete($inbox,$email_number);
                            }
                            if($api["state"]){
                                $countRechazados++;
                            }
                        }
                    }
                }
            }
        } 

        imap_expunge($inbox);
        imap_close($inbox);

        return response()->json(array("RECIBIDOS" => $countRecibidos,"RECHAZADOS" => $countRechazados));
    }

    /**
    * Método para consultar Fecha recepccion en SII
    * @param Emisor formato 17897012-7
    * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
    * @version 2017-08-17
    */
    public function consultarFechaRecepcionSii($Emisor,$tipo_doc,$folio){

        $state = 0;
        $message = "";

        // datos para validar
        $rutEmisor = explode('-', $Emisor); 
        $dte = $tipo_doc.'_'.$folio.'_'.$Emisor.'.xml';


        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
       
        $soap = new \SoapClient($this->wsdl, $options);
       
     
        //Parametros de función ingresarAceptacionReclamoDoc  
        $args = [
            'rutEmisor' => $rutEmisor[0],
            'dvEmisor' => $rutEmisor[1],
            'tipoDoc' => $tipo_doc,
            'folio' => $folio
        ];

        $retry = env('RETRY_CALL');

        for ($i=0; $i<$retry; $i++) {
            try {
                $body = $soap->__soapCall("consultarFechaRecepcionSii",$args);
                break;
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                    $msg .= ': '.$e->getTrace()[0]['args'][1];
                }
                \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                $body = null;
            }
        }
        if ($body===null) {
            \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
            $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

        }else{
            $message = $body;
        }
       
     
        return response()->json(array("state" => $state,"fecha" => $message));

    }

    /**
    * Método para consultar Historial de eventos
    * @param Emisor formato 17897012-7
    * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
    * @version 2017-08-17
    */
    public function listarEventosHistDoc($Emisor,$tipo_doc,$folio){

        $state = 0;
        $message = "";

        // datos para validar
        $rutEmisor = explode('-', $Emisor); 
        $dte = $tipo_doc.'_'.$folio.'_'.$Emisor.'.xml';


        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
        
        $soap = new \SoapClient($this->wsdl, $options);
     
        //Parametros de función ingresarAceptacionReclamoDoc  
        $args = [
            'rutEmisor' => $rutEmisor[0],
            'dvEmisor' => $rutEmisor[1],
            'tipoDoc' => $tipo_doc,
            'folio' => $folio
        ];

        $retry = env('RETRY_CALL');

        for ($i=0; $i<$retry; $i++) {
            try {
                $body = $soap->__soapCall("listarEventosHistDoc",$args);
                break;
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                    $msg .= ': '.$e->getTrace()[0]['args'][1];
                }
                \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                $body = null;
            }
        }
        if ($body===null) {
            \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
            $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);

        }else{
            $message = $body;
        }
       
     
        return response()->json(array("state" => $state,"message" => $message));

    }

    /**
    * Método para consultar Historial de eventos
    * @param Emisor formato 17897012-7
    * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
    * @version 2017-08-17
    */
    public function consultaRechazo($Emisor,$tipo_doc,$folio){

        $state = 0;
        $message = "";

        // datos para validar
        $rutEmisor = explode('-', $Emisor); 
        $dte = $tipo_doc.'_'.$folio.'_'.$Emisor.'.xml';


        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
        
        $soap = new \SoapClient($this->wsdl, $options);
     
        //Parametros de función ingresarAceptacionReclamoDoc  
        $args = [
            'rutEmisor' => $rutEmisor[0],
            'dvEmisor' => $rutEmisor[1],
            'tipoDoc' => $tipo_doc,
            'folio' => $folio
        ];

        $retry = env('RETRY_CALL');

        for ($i=0; $i<$retry; $i++) {
            try {
                $body = $soap->__soapCall("listarEventosHistDoc",$args);
                break;
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                if (isset($e->getTrace()[0]['args'][1]) and is_string($e->getTrace()[0]['args'][1])) {
                    $msg .= ': '.$e->getTrace()[0]['args'][1];
                }
                \sasco\LibreDTE\Log::write(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, \sasco\LibreDTE\Estado::get(\sasco\LibreDTE\Estado::REQUEST_ERROR_SOAP, $msg));
                $body = null;
            }
        }
        if ($body===null) {
            \sasco\LibreDTE\Log::write( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY,  \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry));
            $message = \sasco\LibreDTE\Estado::get( \sasco\LibreDTE\Estado::REQUEST_ERROR_BODY, $this->wsdl, $retry);
        }else{
            $conNC = false;

            if($body->codResp == 15){
                if(!empty($body->listaEventosDoc)){
                    //Verifico si tiene nota de credito
                    foreach ($body->listaEventosDoc as $evento) {
                        if($evento->codEvento == "NCA"){
                            $conNC = true;
                        }
                    }

                    //Si no tiene nota de credito verifico los eventos de rechazo
                    if(!$conNC){
                        if(in_array($evento->codEvento, $this->rechazos) ){
                            //Aquí guardo el evento de rechazo
                            $pdo = \DB::connection('oracle')->getPdo();
                            $p1 = 37;
                            $result = null;

                            $stmt = $pdo->prepare("begin SALFASUR.REPLISTBOXDOMINIO(:ii_codigo, :cv_resultado); end;");
                            $stmt->bindParam(':ii_codigo', $p1, \PDO::PARAM_INT);
                            $stmt->bindParam(':cv_resultado', $p2, \PDO::PARAM_STMT);
                            $stmt->execute();

                         
                            oci_execute($p2, OCI_DEFAULT);
                            oci_fetch_all($p2, $array, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC );
                            oci_free_cursor($p2);

                            return $array;

                            dd($result);
                        }
                    }
                }
            }
        }
       
     
        return response()->json(array("state" => $state,"message" => $message));

    }

    /**
    * Método para dtes pendientes
    * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
    * @version 2017-08-17
    */
    public function getPendientes(){


        $state = 0;
        $message = "Listado de documentos pendientes";
        $documentos_return = array();


        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );

        $soap = new \SoapClient($this->wsdl, $options);
        
        $retry = env('RETRY_CALL');

        //Recorremos toda la carpeta de pendientes
        $folder = Flysystem::connection('local')->get(env('DTES_CARPETA'));
        $sub_folders = $folder->getContents();


        foreach ($sub_folders as $key => $value) {
            if($value["type"] == "dir"){
                $sub_folder = Flysystem::connection('local')->get($value["path"]);
                $dtes = $sub_folder->getContents();
                foreach ($dtes as $dte) {
                    $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
                    $EnvioDte->loadXML(Flysystem::connection('local')->read($dte["path"]));
                    $documentos = $EnvioDte->getDocumentos();
                    foreach ($documentos as $documento) {

                        $rutEmisor = explode('-', $documento->getEmisor());
                        $args = [
                            'rutEmisor' => $rutEmisor[0],
                            'dvEmisor' => $rutEmisor[1],
                            'tipoDoc' => $documento->getTipo(),
                            'folio' => $documento->getFolio()
                        ];

                        $documento = [
                            "FOLIO" => $documento->getFolio(),
                            "EMISOR" => $documento->getEmisor(),
                            "RECEPTOR" => $documento->getReceptor(),
                            "FECHA_EMISION" => $documento->getFechaEmision(),
                            "MONTO_TOTAL" => $documento->getMontoTotal(),
                            "TIO_DOC" => $documento->getTipo()

                        ];

                        array_push($documentos_return,$documento);
                    }
                }
            }
        }
       
     
        return response()->json(array("state" => $state,"message" => $message,"documentos" => $documentos_return));

    }

    /**
    * Método para dtes enviados
    * @author Eduardo Peña Fritz, Salfa Sur (epenafritz[at]gmail.com)
    * @version 2017-08-17
    */
    public function getEnviadas(){

        $state = 0;
        $message = "Listado de documentos pendientes";
        $documentos_return = array();


        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$this->token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
        
        //Obtenemos url del wsdl dependiendo del ambiente
        $wsdl = App::environment('local')?'https://ws2.sii.cl/WSREGISTRORECLAMODTECERT/registroreclamodteservice?wsdl':'https://ws1.sii.cl/WSREGISTRORECLAMODTE/registroreclamodteservice?wsdl';
        $soap = new \SoapClient($wsdl, $options);
        
        $retry = env('RETRY_CALL');

        //Recorremos toda la carpeta de pendientes
        $folder = Flysystem::connection('local')->get(env('DTES_ENVIADOS_CARPETA'));
        $sub_folders = $folder->getContents();


        foreach ($sub_folders as $key => $value) {
            if($value["type"] == "dir"){
                $sub_folder = Flysystem::connection('local')->get($value["path"]);
                $dtes = $sub_folder->getContents();
                foreach ($dtes as $dte) {
                    $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
                    $EnvioDte->loadXML(Flysystem::connection('local')->read($dte["path"]));
                    $documentos = $EnvioDte->getDocumentos();
                    foreach ($documentos as $documento) {

                        $rutEmisor = explode('-', $documento->getEmisor());
                        $args = [
                            'rutEmisor' => $rutEmisor[0],
                            'dvEmisor' => $rutEmisor[1],
                            'tipoDoc' => $documento->getTipo(),
                            'folio' => $documento->getFolio()
                        ];

                        $documento = [
                            "FOLIO" => $documento->getFolio(),
                            "EMISOR" => $documento->getEmisor(),
                            "RECEPTOR" => $documento->getReceptor(),
                            "FECHA_EMISION" => $documento->getFechaEmision(),
                            "MONTO_TOTAL" => $documento->getMontoTotal(),
                            "TIO_DOC" => $documento->getTipo()

                        ];

                        array_push($documentos_return,$documento);
                    }
                }
            }
        }
       
     
        return response()->json(array("state" => $state,"message" => $message,"documentos" => $documentos_return));

    }

    private function getReceptor($Receptor){
        switch ($Receptor) {
            case '86906100-K':
                $receptor = "RAC";
                break;
            case '85297000-6':
                $receptor = "IPL";
                break;
            case '93688000-2':
                $receptor = "SALFA";
                break;
            default:
                return response()->json(array("emisor no corresponde"));
        }

        return $receptor;
    }

    private function getIdByReceptor($Receptor){
        $empresas = $this->empresas;
        if($empresas){
            foreach ($empresas as $empresa) {
                if($empresa["receptor"] == $Receptor){
                    return $empresa["id"];
                }
            }
        }

        return false;
    }

    public function getInfoDoc(){

        $state = 0;
        $message = "Informacion del documento obtenida correctamente";
        $documentos_return = array();

        $file   = env($_GET['type'])."/".$_GET['empresa']."/".$_GET['tipo']."_".$_GET['folio']."_".$_GET['emisor'].".xml";
        
        $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
        $exist = Flysystem::connection('local')->has($file);
        
        if($exist){
            $EnvioDte->loadXML(Flysystem::connection('local')->read($file));
        
            foreach ($EnvioDte->getDocumentos() as $documento) {
                $emision = DateTime::createFromFormat("Y-m-d",$documento->getResumen()['FchDoc'])->format("d/m/Y");
                $venc = DateTime::createFromFormat("Y-m-d",$documento->getResumen()['FVenc'])->format("d/m/Y");
                $rutEmisor = explode('-', $documento->getEmisor());
                $documento = [
                    "AFECTOIVA" => $documento->getResumen()['MntNeto'],
                    "NETO" => $documento->getResumen()['MntNeto'],
                    "EXENTO" => $documento->getResumen()['MntExe'] == false ? 0 : $documento->getResumen()['MntExe'],
                    "IVA" => $documento->getResumen()['MntIVA'],
                    "TOTAL" => $documento->getResumen()['MntTotal'],
                    "FECHAVENCTO" => $venc,
                    "FECHAEMISION" => $emision,
                    "resumen" =>$documento->getResumen()

                ];

                array_push($documentos_return,$documento);
            }
            $state = 1;
        }else{
            $state = 0;
            $message = "No se encontro el archivo";
        }

        return response()->json(array("state" => $state,"message" => $message,"documentos" => $documentos_return));
    }

    public function getInfoDocGMAC($data){

        $state = 0;
        $message = "Informacion del documento obtenida correctamente";
        $documentos_return = array();

        $_GET['empresa'] = $data[':i_empresa'];
        $_GET['tipo']    = $data[':i_tipo'];
        $_GET['folio']   = $data[':i_numero'];
        $_GET['emisor']  = $data[':i_emisor'];
        $_GET['type']    = "DTES_CARPETA";

        $file = env($_GET['type'])."/SALFA/".$_GET['tipo']."_".$_GET['folio']."_".$_GET['emisor'].".xml";
        //$file = env($_GET['type'])."/SALFA/33_369592_86914600-5.xml";
        
        $EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
        $exist = Flysystem::connection('local')->has($file);
        
        if($exist){
            $EnvioDte->loadXML(Flysystem::connection('local')->read($file));
        
            foreach ($EnvioDte->getDocumentos() as $documento) {
                $emision = DateTime::createFromFormat("Y-m-d",$documento->getResumen()['FchDoc'])->format("d/m/Y");
                $venc = DateTime::createFromFormat("Y-m-d",$documento->getResumen()['FVenc'])->format("d/m/Y");
                $rutEmisor = explode('-', $documento->getEmisor());
                $nmbitem = $documento->getResumen()['NmbItem'];
                $nmbitem = intval(preg_replace('/[^0-9]+/', '', $nmbitem), 10); 

                $documento = [
                    "AFECTOIVA" => $documento->getResumen()['MntNeto'],
                    "NETO" => $documento->getResumen()['MntNeto'],
                    "EXENTO" => $documento->getResumen()['MntExe'] == false ? 0 : $documento->getResumen()['MntExe'],
                    "IVA" => $documento->getResumen()['MntIVA'],
                    "TOTAL" => $documento->getResumen()['MntTotal'],
                    "FECHAVENCTO" => $venc,
                    "FECHAEMISION" => $emision,
                    "NMBITEM" => $nmbitem,
                    "DSCITEM" => $documento->getResumen()['DscItem'],
                    //"resumen" => $documento->getResumen()
                ];

                array_push($documentos_return,$documento);
            }
            $state = 1;
        }else{
            $state = 0;
            $message = "No se encontro el archivo";
        }

        return array("state" => $state,"message" => $message,"documentos" => $documentos_return);
        #return response()->json(array("state" => $state,"message" => $message,"documentos" => $documentos_return));
    }

    public function xmlGoSocket($receptor,$tipo,$folio,$emisor,$id){
        $url = "https://render.signature.cl/Render/Xml?DocumentId=$id&PartitionKey=fDIwMTctMTAtMTFUMTU6Mjg6NTQ=&ViewMode=attachment";

        $receptor = $this->getReceptor($receptor);
        $filename = $tipo."_".$folio."_".$emisor.".xml";

        $file  = env('DTES_CARPETA')."/".env('DTES_CARPETA_'.$receptor)."/".$filename;
        $exist = Flysystem::connection('local')->has($file);

        if(!$exist){

            $retry = env('RETRY_CALL');

            for ($i=0; $i<$retry; $i++) {
                try {
                    if(Flysystem::connection('local')->writeStream($file, fopen($url, 'r'))){
                        $state = 1;
                        $msj = "archivo guardado correctamente en la ruta $file";
                        break;
                    }else{
                        $state = -1;
                        $msj = "error al guardar el xml";
                    }
                } catch (\Exception $e) {
                    $msj     = $e;
                    $status  = -2;
                }
            }
        }else{
            $state = 0;
            $msj = "El archivo $filename ha sido descargado previamente";
        }

        return response()->json(["state" => $state, "msj" => $msj]);
    }

    public function getContribuyentes(){
        $token = $this->token;
        //Opciones de ennvio para Cliente Soap. 
        $options = array(
            "exceptions" => 0, 
            "trace" => 1,
            'stream_context' => stream_context_create(
                array(
                    'http' => array(
                        'header' => 'cookie: TOKEN='.$token //Pasamos Token en la cabecera del http
                    )
                )
            )
        );
            
        $soap = new \SoapClient($this->wsdl, $options);
        $firma = new \sasco\LibreDTE\FirmaElectronica($this->config['firma']);
        $state = \sasco\LibreDTE\Sii::PRODUCCION;
        
        $datos = \sasco\LibreDTE\Sii::getContribuyentes($firma, $state, null, $token);
       
        // si hubo errores se muestran
        if (!$datos) {
            foreach (\sasco\LibreDTE\Log::readAll() as $error) {
                echo $error,"\n";
            }
            exit;
        }

        // descargar archivo como CSV
        array_unshift($datos, ['RUT', 'Razón social', 'Número resolución', 'Fecha resolución', 'Email intercambio', 'URL']);
        
        \sasco\LibreDTE\CSV::generate($datos, 'contribuyentes', ';', '');
    }

    public function get_status_doc($rut,$dv,$emisor_rut,$emisor_dv, $dte, $folio, $monto, $fecha){
        $token = $this->token;
        $parametros = [
            'RutConsultante'    => $rut,
            'DvConsultante'     => $dv,
            'RutCompania'       => $emisor_rut,
            'DvCompania'        => $emisor_dv,
            'RutReceptor'       => $rut,
            'DvReceptor'        => $dv,
            'TipoDte'           => $dte,
            'FolioDte'          => $folio,
            'FechaEmisionDte'   => $fecha,
            'MontoDte'          => $monto,
            'token'             => $token,
        ];

        $xml = \sasco\LibreDTE\Sii::request('QueryEstDte', 'getEstDte', $parametros);

        // si el estado se pudo recuperar se muestra
        if ($xml!==false) {
            //print_r((array)$xml->xpath('/SII:RESPUESTA/SII:RESP_HDR')[0]);
            $r = (array)$xml->xpath('/SII:RESPUESTA/SII:RESP_HDR')[0];
            $response = [
                "status" => $r['ERR_CODE'],
                "dte_status" => $r['GLOSA_ESTADO'],
                "msj" => $r['GLOSA_ESTADO'],
                "parametros" => $parametros
            ];

            return  json_encode($response);
        }else{

            return json_decode(-1);
        }        

    }

    public function read_xml($type,$folio,$emisor_rut, $dv){

        $receptor = "SALFA";
        $type = 33;
        $filename = $type."_".$folio."_".$emisor_rut."-".$dv.".xml";
        $xml_route = env('DTES_CARPETA')."/".env('DTES_CARPETA_'.$receptor)."/".$filename;

        $detalle = array();
        $encabezado = array();

        $exist = Flysystem::connection('local')->has($xml_route);

        if($exist){
            $envioDte = new \sasco\LibreDTE\Sii\EnvioDte();
            $envioDte->loadXML(Flysystem::connection('local')->read($xml_route));
            $documentos = $envioDte->getDocumentos();

            $enc = $documentos[0]->getDatos()['Encabezado'];
            $det = $documentos[0]->getDatos()['Detalle'];

            $encabezado = array(
                "TIPO" => $type,
                "FOLIO" =>  $folio,
                "EMISION" =>  $enc['IdDoc']['FchEmis'],
                "VENCIMIENTO" =>  $enc['IdDoc']['FchVenc'],
                "CODIGORECEP" => $enc['Receptor']['CdgIntRecep'],#AQUI HAY QUE TRADUCIR EL CODIGO
                "NETO" =>  isset($enc['Totales']['MntNeto'])? $enc['Totales']['MntNeto']: 0,
                "EXENTO" =>  isset($enc['Totales']['MntExe'])?$enc['Totales']['MntExe']:0,
                "IVA" =>  isset($enc['Totales']['IVA'])?$enc['Totales']['IVA']:0,
                "TOTAL" =>  $enc['Totales']['MntTotal'],
            );

            foreach ($det as $key => $value) {
                if(is_numeric($key)){
                     $temp = array(
                        "TIPO" => $type,
                        "FOLIO" => $folio,
                        "LINEA" => $value['NroLinDet'],
                        "PRODUCTOID" => $value['CdgItem']['VlrCodigo'],
                        "CANTIDAD" => $value['QtyItem'],
                        "PRECIO" => $value['PrcItem'],
                        "VALOR" => $value['MontoItem'],
                    );

                     array_push($detalle, $temp);
                }else{
                     $temp = array(
                        "TIPO" => $type,
                        "FOLIO" => $folio,
                        "LINEA" => $det['NroLinDet'],
                        "PRODUCTOID" => $det['CdgItem']['VlrCodigo'],
                        "CANTIDAD" => $det['QtyItem'],
                        "PRECIO" => $det['PrcItem'],
                        "VALOR" => $det['MontoItem'],
                    );

                     array_push($detalle, $temp);
                     break;
                }
            }
        }
        
        return json_encode(array("encabezado" => $encabezado, "detalle" => $detalle));
    }
                  
    public function downloadXmlGosocket($receptor,$tipo,$folio,$emisor,$url){
        $url = "https://api.signature.cl/webservices/GetDocument.aspx?p=$url";
        $receptor = $this->getReceptor($receptor);
        $filename = $tipo."_".$folio."_".$emisor.".xml";

        $file  = env('DTES_CARPETA')."/".env('DTES_CARPETA_'.$receptor)."/".$filename;
        $exist = Flysystem::connection('local')->has($file);

        if(!$exist){

            $retry = env('RETRY_CALL');

            for ($i=0; $i<$retry; $i++) {
                try {
                    if(Flysystem::connection('local')->writeStream($file, fopen($url, 'r'))){
                        $state = 1;
                        $msj = "archivo guardado correctamente en la ruta $file";
                        break;
                    }else{
                        $state = -1;
                        $msj = "error al guardar el xml";
                    }
                } catch (\Exception $e) {
                    $msj     = $e;
                    $status  = -2;
                }
            }
        }else{
            $state = 0;
            $msj = "El archivo $filename ha sido descargado previamente";
        }

        return response()->json(["state" => $state, "msj" => $msj]);
    }

}


