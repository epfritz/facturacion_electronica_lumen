<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'HomeController@index');
$app->get('/mailDownload/', 'SiiController@mailDownload');
$app->get('/acuseReciboMercaderia/{Emisor}/{tipo_doc}/{folio}/{Receptor}', 'SiiController@acuseReciboMercaderia');
$app->get('/aceptaDocumento/{Emisor}/{tipo_doc}/{folio}/{Receptor}', 'SiiController@aceptaDocumento');
$app->get('/consultarFechaRecepcionSii/{Emisor}/{tipo_doc}/{folio}', 'SiiController@consultarFechaRecepcionSii');
$app->get('/listarEventosHistDoc/{Emisor}/{tipo_doc}/{folio}', 'SiiController@listarEventosHistDoc');
$app->get('/consultaRechazo/{Emisor}/{tipo_doc}/{folio}', 'SiiController@consultaRechazo');
$app->get('/acuseReciboMercaderiaMasivo', 'SiiController@acuseReciboMercaderiaMasivo');
$app->get('/getPendientes', 'SiiController@getPendientes');
$app->get('/getEnviadas', 'SiiController@getEnviadas');
$app->get('/check/{Emisor}/{tipo_doc}/{folio}', 'SiiController@checkAcuse');
$app->get('/getSendTotal', 'SiiController@getSendTotal');
$app->get('/getInfoDoc', 'SiiController@getInfoDoc');
$app->get('/getInfoDocGMAC', 'SiiController@getInfoDocGMAC');
$app->get('/xmlGoSocket/{receptor}/{tipo}/{folio}/{emisor}/{id}', 'SiiController@xmlGoSocket');
$app->get('/getContribuyentes', 'SiiController@getContribuyentes');
$app->get('/check_conection', 'SiiController@check_conection');
$app->get('/get_status_doc/{rut}/{dv}/{emisor_rut}/{emisor_dv}/{dte}/{folio}/{monto}/{fecha}', 'SiiController@get_status_doc');
$app->get('/read_xml/{type}/{folio}/{emisor_rut}/{dv}', 'SiiController@read_xml');
$app->get('/downloadXmlGosocket/{receptor}/{tipo}/{folio}/{emisor}/{url}', 'SiiController@downloadXmlGosocket');
